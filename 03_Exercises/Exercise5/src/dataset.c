/* ------------------------------ Includes ------------------------------ */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "dataset.h"

/* -------------------- Exported Function Prototypes -------------------- */
double DSNode_getAverage(DSNode *self) {
    return (self->low + self->high) / 2;
}

double DSNode_getChange(DSNode *self) {
    /* Check if we have any previous data */
    if(self->previous != NULL) {
        /* Calculate the change in average */
        return DSNode_getAverage(self) - DSNode_getAverage(self->previous);
    } else {
    	/* Return 0 if there's no previous data */
        return 0;
    }
}

void DSList_append(DSList* *self, DSNode node) {
    /* Construct a new list object */
    DSList *newList = calloc(1, sizeof(DSList));

    /* Add the node to the list object */
    newList->node = node;

    /* Check if the list is empty */
    if(*self == NULL) {
        /* Start a new list */
        *self = newList;
    } else {
        DSList* iterator = *self;

        /* Go to the end of the list */
        while(iterator->next != NULL) {
            iterator = iterator->next;
        }

        /* Remember the previous node */
        newList->node.previous = &iterator->node;

        /* Add the new list object at the end */
        iterator->next = newList;
    }
}

void DSList_clear(DSList *self) {
    /* Check if the list is empty */
    if(self == NULL) {
        printf("Error: Could not clear the allocated memory for the list, the list is empty.\n");
    }

    /* Check if there's a next entry in the list */
    if(self->next != NULL) {
        /* Clear the next entry in the list as well */
        DSList_clear(self->next);
    }

    /* Free allocated memory for the list entry */
    free(self);
}

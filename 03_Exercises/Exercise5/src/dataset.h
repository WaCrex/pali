#ifndef _DATASET_H_
#define _DATASET_H_

/* ------------------------------ Includes ------------------------------ */
#include <time.h>

/* ------------------------ Constants and Types ------------------------- */
#define CREATE_DSNODE   (DSNode) {{.tm_year=0, .tm_mon=0, .tm_mday=0}, 0, 0, NULL}

typedef struct DSNode {
    /* --------- Variables ---------- */
    struct tm date;
    double low;
    double high;

    struct DSNode *previous;
} DSNode;

typedef struct DSList {
    /* --------- Variables ---------- */
    DSNode node;

    struct DSList *next;
} DSList;


/* -------------------- Exported Function Prototypes -------------------- */
double DSNode_getAverage(DSNode *self);
double DSNode_getChange(DSNode *self);
void DSList_append(DSList* *self, DSNode node);
void DSList_clear(DSList *self);

#endif /* _DATASET_H_ */

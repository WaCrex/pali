/* ------------------------------ Includes ------------------------------ */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataset.h"

/* ---------------------- Local Function Prototypes --------------------- */
void readDataFile(DSList **dataset, const char* filename);
void writeDataFile(DSList *dataset, const char* filename);

/* -------------------- Exported Function Prototypes -------------------- */
int main()
{
    /* Create a new data list */
    DSList *dataset = NULL;

    /* Read the data in data.csv into the list */
    readDataFile(&dataset, "data.csv");

    /* Process and write the data to output.csv */
    writeDataFile(dataset, "output.csv");

    /* Finally, do some cleanup */
    DSList_clear(dataset);
}

/* --------------------------- Local Functions -------------------------- */
void readDataFile(DSList **dataset, const char* filename)
{
    FILE *fp;

    /* Open the datafile */
    if((fp = fopen (filename, "r")) == NULL)
    {
        printf("Error: The file '%s' was not found!\n", filename);
        exit(1);
    }

    int row = 1;
    char buffer[64];

    /* Read file until end of file */
    while(fscanf(fp, "%s", buffer) != EOF)
    {
        /* Create a new node */
        DSNode node = CREATE_DSNODE;

        /* Read data into the node */
        if(sscanf(buffer, "%4d-%2d-%2d,%lf,%lf",
            &node.date.tm_year,
            &node.date.tm_mon,
            &node.date.tm_mday,
            &node.low,
            &node.high
        ) == 5)
        {
            /* Add the node to the list */
            DSList_append(dataset, node);
        } else {
            printf("Error: The data '%s' on row %d could not be parsed.\n", buffer, row);
        }

        ++row;
    }

    /* Close the datafile */
    fclose(fp);
}

void writeDataFile(DSList *dataset, const char* filename)
{
    /* Open the datafile */
    FILE *fp = fopen (filename, "w");

    /* Iterate through the list */
    for(DSList* iterator = dataset; iterator != NULL; iterator = iterator->next)
    {
        /* Obtain the data node */
        DSNode* node = &iterator->node;

        /* Write the data to the file */
        fprintf(fp, "%04d-%02d-%02d,%.01lf,%.01lf\n",
            node->date.tm_year, node->date.tm_mon, node->date.tm_mday,

            /* Get the average of node.low & node.high */
            DSNode_getAverage(node),

            /* Get the average difference from the previous node */
            DSNode_getChange(node)
        );
    }

    /* Close the datafile */
    fclose(fp);
}

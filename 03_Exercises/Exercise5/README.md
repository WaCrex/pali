### Exercise 5 - The Counter

In this assignment the student is to do an application that goes thru large sets of data.

In the file attached there is a large dataset of exchange rates of a specific currency. The application the student shall create will read all data and store it in memory. When it is stored in memory there should be a function that calculates the average and change in exchange rate from the previous day., this added data should be included and written to a new file. Ex:

```markdown
Input format:                                 Output format: 
    2018-02-28;11.2;12.8                  2018-02-28;12.0;0.0       #0.0 in change indicates no previous data.
    2018-03-01;11.1;12.3                  2018-03-01;11.7;-0.3
```

The purpous of this assignment is to let the student show that it can handle data and I/O from and to files.



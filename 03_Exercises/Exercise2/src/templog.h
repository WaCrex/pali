#ifndef _TEMPLOG_H_
#define _TEMPLOG_H_

#include <stdint.h>
#include <time.h>

typedef struct
{
	uint16_t id;
	struct tm date;
	double minTemp;
	double maxTemp;
} tempValue_t;

#endif //_TEMPLOG_H_

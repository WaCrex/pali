#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include "templog.h"
#include "data.h"

void printMonthlyData(void);
void printRecordDays(void);
void printTemp(char const *prefix, double temp, char const *suffix);

//extern tempValue_t temps[];

const char* month[12] = {
	"JANUARY", "FEBRUARY", "MARS", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"
};

int main()
{
	printMonthlyData();
	printf("\n");
	printRecordDays();

	return 0;
}

/* Prints a summary of each month in data,
   highest, lowest and average temp. */
void printMonthlyData()
{
	double maxTemp, minTemp, avgTemp;
	tempValue_t *previous, *current;
	char const *line = "======================================";
	printf("%s[ Monthly Data ]%s=\n", line, line);

	for(int i=0,days=0; i<=(int) (sizeof(temps)/sizeof(tempValue_t)); ++i, ++days)
	{
		current = (temps+i);

		/* Check if it's a new month */
		if(i!=0 && previous->date.tm_mon != current->date.tm_mon) {
			/* Print the data for previous month */
			printf("[ %d %9s -", previous->date.tm_year, month[previous->date.tm_mon]);
			printTemp(" Max: ", maxTemp, " -");
			printTemp(" Min: ", minTemp, " -");
			printTemp(" Avg: ", avgTemp/days, " ]\n");

			/* Reset day count */
			days = 0;
		}

		/* re-/Initialize Temperature Variables */
		if(days == 0) {
			maxTemp = current->maxTemp;
			minTemp = current->minTemp;
			avgTemp = 0;
		}

		/* Check if it's this month's lowest temp */
		if(current->minTemp < minTemp) {
			minTemp = current->minTemp;
		}

		/* Check if it's this month's highest temp */
		if(current->maxTemp > maxTemp) {
			maxTemp = current->maxTemp;
		}

		/* Store this days average temp */
		avgTemp += (current->minTemp + current->maxTemp)/2;

		previous = current;
	}
}

void printTemp(char const *prefix, double temp, char const *suffix) {
	printf("%s%6.2f°C/%6.2f°F%s", prefix, temp, ((temp*9)/5)+32, suffix);
}

/* Prints the dates of the hottest and coldest day and
   days with least and largest temperature range */
void printRecordDays()
{
	tempValue_t *maxTemp, *minTemp, *maxRange, *minRange, *previous, *current;
	char const *line = "=======================================";

	printf("%s[ Record Days ]%s\n", line, line);
	for(int i=0,days=0; i<=(int)(sizeof(temps)/sizeof(tempValue_t)); ++i, ++days)
	{
		current = (temps+i);

		/* Check if it's a new month */
		if(i!=0 && previous->date.tm_mon != current->date.tm_mon) {
			/* Print the data for previous month */
			printf("%d/%02d  ", previous->date.tm_year, previous->date.tm_mon+1);
			printf("H.Day: %02d @ %5.2f°C  ", maxTemp->date.tm_mday, maxTemp->maxTemp);								/* Print hottest day of the month */
			printf("C.Day: %02d @ %5.2f°C  ", minTemp->date.tm_mday, minTemp->minTemp);								/* Print coldest day of the month */
			printf("S.Diff: %02d @ %5.2f°C  ", minRange->date.tm_mday, (minRange->maxTemp - minRange->minTemp));	/* Print day with smallest temperature difference */
			printf("L.Diff: %02d @ %5.2f°C\n", maxRange->date.tm_mday, (maxRange->maxTemp - maxRange->minTemp));	/* Print day with largest temperature difference */

			/* Reset day count */
			days = 0;
		}

		/* re-/Initialize Temperature Variables */
		if(days == 0) {
			maxTemp = current;
			minTemp = current;

			maxRange = current;
			minRange = current;
		}

		/* Check if it's this month's lowest temp */
		if(current->minTemp < minTemp->minTemp) {
			minTemp = current;
		}

		/* Check if it's this month's highest temp */
		if(current->maxTemp > maxTemp->maxTemp) {
			maxTemp = current;
		}

		/* Check if it's this month's smallest temp range */
		if((current->maxTemp - current->minTemp) < (minRange->maxTemp - minRange->minTemp)) {
			minRange = current;
		}

		/* Check if it's this month's largest temp range */
		if((current->maxTemp - current->minTemp) > (maxRange->maxTemp - maxRange->minTemp)) {
			maxRange = current;
		}

		previous = current;
	}
}

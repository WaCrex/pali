### Exercise 2 - Data Handling

In this assignment the student is to do an application that goes thru large data.

In the files attached there are two function stubs that shall be completed.
These functions shall calculate the maximum, minimum and average temperature over each month (see the data.c file for data layout), and the show the days that have the lowest temperature, the day with the highest temperature, the day with the largest span between lowest and higest temp and the day that has the least span.

Printout should be in a neat and easy to read way.

The purpous of this assignment is to let the student show that it can handle data with mixed types and complex loops.

```c
#ifndef _TEMPLOG_H_
#define _TEMPLOG_H_

#include <stdint.h>
#include <time.h>

typedef struct
{
	uint16_t id;
	struct tm date;
	double maxTemp;
	double minTemp;
} tempValue_t;

#endif //_TEMPLOG_H_
```

```c
#include "templog.h"
#define DATE_SIZE            1096

tempValue_t temps[DATE_SIZE] =
{
    { 0, { .tm_year = 2015, .tm_mon = 0, .tm_mday = 1 }, 14.04, 32.34 },
    { 1, { .tm_year = 2015, .tm_mon = 0, .tm_mday = 2 }, 30.42, 44.02 },
    { 2, { .tm_year = 2015, .tm_mon = 0, .tm_mday = 3 }, 28.28, 40.28 },
    { 3, { .tm_year = 2015, .tm_mon = 0, .tm_mday = 4 }, 17.25, 34.55 },
    { 4, { .tm_year = 2015, .tm_mon = 0, .tm_mday = 5 }, 15.41, 28.41 },
    { 5, { .tm_year = 2015, .tm_mon = 0, .tm_mday = 6 }, 5.32, 9.52 },
	...
};
#endif //_DATA_H_
```

```c
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "templog.h"
#include "data.h"

void printMonthlyData(void);
void printRecordDays(void);

extern tempValue_t temps[];

int main(int argc, char* argv[])
{
	printMonthlyData();
	printRecordDays();

	return 0;
}

/* Prints a summary of each month in data,
   highest, lowest and average temp. */
void printMonthlyData()
{

}

/* Prints the dates of the hottest and coldest day and
   days with least and largest temperature range */
void printRecordDays()
{

}
```

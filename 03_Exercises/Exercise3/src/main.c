#include <stdio.h>

//Interface of funtion to make.
void swap(int* val1, int* val2);

int main()
{
	//Values to be swapped
	int value1 = 42;
	int value2 = 73;

	printf("Before swap value1 is %d and value2 is %d\n", value1, value2);

	//Call swap function here
    swap(&value1, &value2);

	printf("After swap value1 is %d and value2 is %d\n", value1, value2);

	return 0;
}

void swap(int* val1, int* val2)
{
    int temp = *val1;
    *val1 = *val2;
    *val2 = temp;
}

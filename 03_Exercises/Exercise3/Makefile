# Environment setup
MD:= mkdir -p
CC:=gcc
LD:=gcc

SRC_DIR:=src
OBJ_DIR:=obj
BIN_DIR:=bin

SRCS:= $(wildcard $(SRC_DIR)/*.c)
OBJS:= $(patsubst %.c, $(OBJ_DIR)/%.o, $(SRCS))
DEPS:= $(patsubst %.o, %.d, $(OBJS))

OUTPUT:= $(BIN_DIR)/exercise3.exe

WARNINGS := -Wall -Wextra -Wpedantic -Wshadow -Wpointer-arith -Wcast-align \
		-Wwrite-strings -Wmissing-prototypes -Wmissing-declarations \
		-Wredundant-decls -Wnested-externs -Winline -Wno-long-long \
		-Wuninitialized -Wconversion -Wno-variadic-macros \
		-Wswitch-enum -Wswitch-default
CFLAGS:= -g

.PHONY: all clean

all: $(OUTPUT)
	@echo "Done!"

$(OUTPUT): $(OBJS) force_look
	@echo \*\*\*\*\*\* Creating executable: $(OUTPUT)
	-@$(MD) $(dir $@)
	@gcc $(CFLAGS) -std=gnu99 $(WARNINGS) $(OBJS) -o $@

run:
	@$(OUTPUT)

clean:
	-@$(RM) -rd $(BIN_DIR)
	-@$(RM) -rd $(OBJ_DIR)
	-@$(RM) *~ $(SRC_DIR)/*~

$(OBJ_DIR)/%.o: %.c Makefile
	@echo \*\*\*\*\*\* Building: $@ #from $<
	-@$(MD) $(dir $@)
	@$(CC) $(CFLAGS) -std=gnu99 $(WARNINGS) -MMD -c $< -o $@

force_look:
	@true

-include $(DEPS)

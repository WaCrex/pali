### Exercise 3 - Swapping

In this assignment the student is supposed to complete the appended C code to finish the program.

The purpouse of the program is to swap two number in a "call-by-reference" nature.
There are a function call to implement and a function to write to do the actual swapping.

The purpouse of this assignment is to let the student show how to handle pointers.

```c
#include <stdio.h>

//Interface of funtion to make.
void swap(int* val1, int* val2);

int main()
{
	//Values to be swapped
	int value1 = 42;
	int value2 = 73;

	printf("Before swap value1 is %d and value2 is %d\n", value1, value2);

	//Call swap function here


	printf("After swap value1 is %d and value2 is %d\n", value1, value2);

	return 0;
}

void swap(int* val1, int* val2)
{

}
```

/***************************** Includes ******************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/************************ Constants and Types ************************/
typedef struct measurement{
	char id[16];
	double value;
} measurement_t;

typedef struct link{
	struct measurement datapoint;
	struct link* next;
} link_t;

/***************************** Variables *****************************/
link_t* head;

/********************* Local Function Prototypes *********************/
void printAll(link_t* start);
void addToList(link_t** start, link_t* data);
void clearList(link_t* start);

/************************* Exported Functions ************************/
int main()
{
    char id[16];
    double value;
    char opt;
    int shouldExit = 0;

    head = NULL;

    printf("Please enter ID (char[16]) & Value (double):\n");
    while(!shouldExit){
        printf("\\:> ");

        if(scanf("%16s %lf", id, &value) == 2) {
            /* Allocate some memory for the new entry */
            link_t* new = (link_t*) calloc(1, sizeof(link_t));

            /* Assign the values read */
            strncpy(new->datapoint.id, id, sizeof(id));
            new->datapoint.value = value;

            /* Add the entered data to the list */
            addToList(&head, new);

            /* Ask if we should continue */
            printf("Do you want to add more (y/n)? ");
            if(scanf(" %c", &opt)) {
                switch(opt) {
                    case 'y':
                        break;
                    case 'n':
                        shouldExit = 1;
                        break;
                    default:
                        printf("Invalid entry, please enter either 'y' or 'n'.");
                        break;
                }
            }
        }
    }

    /* Print all data in list */
    printAll(head);

    /* And finally clear the list */
    clearList(head);

	return 0;
}

/************************** Local Functions **************************/
void printAll(link_t* start)
{
	link_t* iter = start;
	printf("Data: \n");
	while(iter != NULL){
		printf("Id: %16s\nData: %2.2f\n\n", iter->datapoint.id, iter->datapoint.value);
		iter = iter->next;
	}
}

void addToList(link_t** start, link_t* data)
{
	link_t* iter = *start;
	if(*start == NULL){
		*start = data;
	}else{
		while(iter->next != NULL){
			iter = iter->next;
		}
		iter->next = data;
	}
}

void clearList(link_t* start)
{
    /* Clear the next entry in the list as well */
    if(start->next != NULL) {
        clearList(start->next);
    }

    free(start);
}

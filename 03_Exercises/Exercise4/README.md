### Exercise 4 - The List

In this assignment the student is supposed to make a program that stores datapoints.

The purpouse of the program is gather data from the keyboard and store it to a linked list.
It should be possible to indefinetly enter data untill a specific key is pressed (ex. meny option).
The format of data and a stub is provided in the appended file.

The purpouse of this assignment is to let the student show how to handle pointers, linked lists and data entry.

```c
/***************************** Includes ******************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/************************ Constants and Types ************************/
typedef struct measurement{
	char id[16];
	double value;
} measurement_t;

typedef struct link{
	struct measurement datapoint;
	struct link* next;
}link_t;

/***************************** Variables *****************************/


/********************* Local Function Prototypes *********************/
void printAll(link_t* start);

/************************* Exported Functions ************************/
int main()
{


	return 0;
}

/************************** Local Functions **************************/
void printAll(link_t* start)
{
	link_t* iter = start;
	printf("Data: \n");
	while(iter != NULL){
		printf("Id: %16s\nData: %2.2f\n\n", iter->datapoint.id, iter->datapoint.value);
		iter = iter->next;
	}
}
```

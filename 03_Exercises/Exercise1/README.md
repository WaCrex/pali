### Exercise 1 - The Matrix

In this assignment the student is to do an application that calculates the sums of an matix.
The design of the printout and the data to be used is in the file below.

The purpous of this assignment is to let the student show that it can handle simple printout of data with formatting and simple loops.

```c
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

void printSums(void);

uint8_t data[10][10] = {
	{87, 248, 175, 42, 226, 245, 58, 63, 189, 62},
	{58, 22, 56, 239, 49, 112, 222, 185, 228, 144},
	{29, 119, 238, 187, 135, 89, 54, 179, 38, 5},
	{196, 173, 148, 40, 41, 57, 205, 232, 206, 126},
	{96, 46, 228, 109, 71, 225, 23, 51, 136, 208},
	{9, 7, 191, 88, 108, 54, 5, 105, 205, 31},
	{97, 147, 63, 139, 149, 5, 104, 111, 102, 199},
	{231, 121, 127, 198, 207, 50, 14, 254, 71, 230},
	{85, 89, 10, 61, 150, 15, 205, 242, 160, 43},
	{164, 42, 30, 202, 222, 44, 153, 211, 212, 112}
};

int main()
{
	printf("Here are the sum of the data: \n");
	printSums();

	return 0;
}

void printSums()
{
}

/*
The output should look like:

	Here are the sum of the data:
	  87  248  175   42  226  245   58   63  189   62 | 1395
	  58   22   56  239   49  112  222  185  228  144 | 1315
	  29  119  238  187  135   89   54  179   38    5 | 1073
	 196  173  148   40   41   57  205  232  206  126 | 1424
	  96   46  228  109   71  225   23   51  136  208 | 1193
	   9    7  191   88  108   54    5  105  205   31 |  803
	  97  147   63  139  149    5  104  111  102  199 | 1116
	 231  121  127  198  207   50   14  254   71  230 | 1503
	  85   89   10   61  150   15  205  242  160   43 | 1060
	 164   42   30  202  222   44  153  211  212  112 | 1392
	--------------------------------------------------------
	1052 1014 1266 1305 1358  896 1043 1633 1547 1160 |12274
*/
```

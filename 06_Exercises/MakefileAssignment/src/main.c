/********************************* Includes **********************************/
#include <stdio.h>

#include "ExtLib.h"
#include "SmallComponent2.h"
#include "SmallComponent1.h"

/**************************** Constants and Types ****************************/

/********************************* Variables *********************************/

/************************* Local Function Prototypes *************************/

/***************************** Exported Functions ****************************/
int main(int argc, char* argv[])
{
	int num = 4;
	int num1 = ExtLib_FuncCall1(num);
	printf("%d is twice %d\n", num1, num);

	SmallComponent1_FuncCall1("Eric Lind");
	int num2 = SmallComponent2_FuncCall2();
	printf("\n%d Letters\n", num2);
}

/****************************** Local Functions ******************************/
#ifndef _SMALLCOMPONENT2_H_
#define _SMALLCOMPONENT2_H_
/********************************* Includes **********************************/

/**************************** Constants and Types ****************************/

/********************************* Variables *********************************/

/*********************** Exported Function Prototypes ************************/
void SmallComponent2_FuncCall1(char letter);
int SmallComponent2_FuncCall2(void);

#endif //_SMALLCOMPONENT2_H_
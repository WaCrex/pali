/********************************* Includes **********************************/
#include <stdio.h>
#include "SmallComponent1.h"
#include "SmallComponent2.h"

/**************************** Constants and Types ****************************/

/********************************* Variables *********************************/

/************************* Local Function Prototypes *************************/

/***************************** Exported Functions ****************************/
void SmallComponent1_FuncCall1(char* text)
{
	char* ptr = text;
	while(*ptr != '\0'){
		SmallComponent2_FuncCall1(*ptr);
		ptr++;
	}
}

/****************************** Local Functions ******************************/

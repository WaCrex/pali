#ifndef _SMALLCOMPONENT1_H_
#define _SMALLCOMPONENT1_H_
/********************************* Includes **********************************/

/**************************** Constants and Types ****************************/

/********************************* Variables *********************************/

/*********************** Exported Function Prototypes ************************/
void SmallComponent1_FuncCall1(char* text);

#endif //_SMALLCOMPONENT1_H_
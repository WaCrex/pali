/********************************* Includes **********************************/
#include <stdio.h>
#include "SmallComponent2.h"
/**************************** Constants and Types ****************************/

/********************************* Variables *********************************/
static int letterCount = 0; 

/************************* Local Function Prototypes *************************/

/***************************** Exported Functions ****************************/
void SmallComponent2_FuncCall1(char letter)
{
	printf("%c", letter);
	letterCount++;
}

int SmallComponent2_FuncCall2(void)
{
	return letterCount;
}

/****************************** Local Functions ******************************/

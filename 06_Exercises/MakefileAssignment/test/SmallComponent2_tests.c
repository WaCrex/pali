/********************************* Includes **********************************/
#include "SimpleTest.h"
#include "SmallComponent2.h"

/**************************** Constants and Types ****************************/

/********************************* Variables *********************************/

/************************* Local Function Prototypes *************************/

/***************************** Exported Functions ****************************/
SimpleTest_Initialize();
SimpleTest_CreateTest(SmallComponent2_Test_Initialized)
{
    // Prepare our data to be monitored
    int value = 999;

    // Call the component
    value = SmallComponent2_FuncCall2();

    // What should we check against here?
    SimpleTest_AssertInteger(0, value, "Initial value is not 0");
}
SimpleTest_FinalizeTest()

SimpleTest_CreateTest(SmallComponent2_Test_ThreeLetters)
{
    // Prepare our data to be monitored
    int value = 999;

    // Call the component
    SmallComponent2_FuncCall1('H');
    SmallComponent2_FuncCall1('E');
    SmallComponent2_FuncCall1('J');
    value = SmallComponent2_FuncCall2();

    // What should we check against here?
    SimpleTest_AssertInteger(3, value, "Value after 3 iterations is not 3");
}
SimpleTest_FinalizeTest()

static char* runTests()
{
    SimpleTest_RunTest(SmallComponent2_Test_Initialized);
    SimpleTest_RunTest(SmallComponent2_Test_ThreeLetters);
    return NULL;
}

int main(int argc, char **argv)
{
    const char* testsResult =  runTests();
    printf("%s\n", (testsResult == NULL) ? "All tests passed" : testsResult);
    printf("Tests run: %d\n", SimpleTestCounter);
    return (testsResult != NULL);
}

/****************************** Local Functions ******************************/


/********************************* Includes **********************************/
#include "SimpleTest.h"
#include "SmallComponent1.h"
#include "SmallComponent2.h"

/**************************** Constants and Types ****************************/

/********************************* Variables *********************************/
char retVal[4] = {'\0', '\0', '\0', '\0'};
int letterCount = 0;

/************************* Local Function Prototypes *************************/

/***************************** Exported Functions ****************************/
SimpleTest_Initialize();
SimpleTest_CreateTest(SmallComponent1_Test1)
{
    // Prepare our data to be monitored
    char* text = "HEJ";

    // Call the component
    SmallComponent1_FuncCall1(text);

    // What should we check against here?
    SimpleTest_AssertInteger('H', retVal[0], "Letter 1 value is not corresponding");
    SimpleTest_AssertInteger(3, letterCount, "Number of letters is not corresponding");


}
SimpleTest_FinalizeTest()

static char* runTests()
{
    SimpleTest_RunTest(SmallComponent1_Test1);
    return NULL;
}

int main(int argc, char **argv)
{
    const char* testsResult =  runTests();
    printf("%s\n", (testsResult == NULL) ? "All tests passed" : testsResult);
    printf("Tests run: %d\n", SimpleTestCounter);
    return (testsResult != NULL);
}

/******************************* Mock Functions ******************************/
void SmallComponent2_FuncCall1(char letter)
{
	retVal[letterCount] = letter;
	++letterCount;
}

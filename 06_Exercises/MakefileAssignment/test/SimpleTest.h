#ifndef SIMPLETEST_H
#define SIMPLETEST_H
/********************************* Includes **********************************/
#include <string.h>
#include <stdio.h>
#include <stdint.h>

/**************************** Constants and Types ****************************/
 
/********************************* Variables *********************************/
extern int SimpleTestCounter;

/***************************** Exported Functions ****************************/
#define SimpleTest_AssertString(StringA, StringB, Message) \
	do { \
		sprintf(retString, "%s: %s - Expected \"%s\" got \"%s\"", __FUNCTION__, Message, StringA, StringB); \
		if (strcmp(StringA, StringB) != 0) \
			return retString; \
	} while (0)

#define SimpleTest_AssertInteger(IntA, IntB, Message) \
	do { \
		sprintf(retString, "%s: %s - Expected %d got %d", __FUNCTION__, Message, (uint32_t)IntA, (uint32_t)IntB); \
		if (IntA != IntB) \
			return retString; \
	} while (0)

#define SimpleTest_RunTest(test) \
	do { \
		char *message = test(); \
		SimpleTestCounter++; \
		if (message) \
			return message; \
	} while (0)

#define SimpleTest_Initialize() int SimpleTestCounter = 0u; char retString[1024];
#define SimpleTest_CreateTest(Name) static char* Name(void) {
#define SimpleTest_FinalizeTest() return NULL; }


/* Based on http://www.jera.com/techinfo/jtns/jtn002.html */
#endif
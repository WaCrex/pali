/********************************* Includes **********************************/
#include "SimpleTest.h"
#include "ExtLib.h"

/**************************** Constants and Types ****************************/

/********************************* Variables *********************************/

/************************* Local Function Prototypes *************************/

/***************************** Exported Functions ****************************/
SimpleTest_Initialize();
SimpleTest_CreateTest(ExtLib_Test1)
{
    // Prepare our data to be monitored
    int value = 8;

    // Call the component, this will
    // run the complete BlinkOnce logic and return
    int retval = ExtLib_FuncCall1(value);

    // What should we check against here?
    SimpleTest_AssertInteger((value*2), retval, "Value is not corresponding");
}
SimpleTest_FinalizeTest()

static char* runTests()
{
    SimpleTest_RunTest(ExtLib_Test1);
    return NULL;
}

int main(int argc, char **argv)
{
    const char* testsResult =  runTests();
    printf("%s\n", (testsResult == NULL) ? "All tests passed" : testsResult);
    printf("Tests run: %d\n", SimpleTestCounter);
    return (testsResult != NULL);
}

/****************************** Local Functions ******************************/

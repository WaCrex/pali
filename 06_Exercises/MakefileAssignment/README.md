## Make a Makefile - Examination assignment
In this assignment you will create a Makefile.
I have included all files you will need to be able to compile a working program.
There is also simple test for all components included. 

**Your assignment is:**

1. To create a Makefile with a all target to compile the application called super-program. 
   All folders that are missing shall be created.
2. Add target for test. The test target shall build all test executables and run them.
3. Add a deploy target that copies the executable and all files from /docs to the deploy folder.
4. Add a clean target that cleans all build folders and the deploy folder.

```
Base File Structure 
./
 |- build*/                        #Place all objectfiles and super-program executable here
 |- deploy*/                       #Copy the deploy files here
 |- docs/
 |   |- LICENSE.md
 |   \- README.md
 |- libs/
 |   |- ExtLib.c                   #To be included in super-program executable
 |   \- ExtLib.h
 |- src/
 |   |- main.c                     #To be included in super-program executable
 |   |- SmallComponent1.c          #To be included in super-program executable
 |   |- SmallComponent1.h
 |   |- SmallComponent2.c          #To be included in super-program executable
 |   \- SmallComponent2.h
 |- test/
 |   |- ExtLib-tests.c
 |   |- SimpleTest.h
 |   |- SmallComponent1_tests.c
 |   |- SmallComponent2_tests.c
 |   \- build*/                    #Place all test object and executable files here
```
(* This folder needs to be created before any files ar written here)

To copy file you need the cp command. Ex:
```
cp build/super-program ./deploy/
```
This command copies the file super-program from the build folder to the deploy folder.
Everything else you need for this task is available from the Makefile slides/examples.

For a G you will need to create the easiest Makefile to acomplish these tasks. (make all, deploy, test, clean)
For a VG I need to see some improvements from the basic implementation.

I wish you best of luck!

#ifndef MOCKED_ARDUINO_H
#define MOCKED_ARDUINO_H
/********************************* Includes **********************************/
#include <stdint.h>

/**************************** Constants and Types ****************************/
#define HIGH 0x1
#define LOW  0x0

/***************************** Exported Functions ****************************/
void digitalWrite(uint8_t pin, uint8_t val);
void delay(unsigned long milliseconds);

#endif
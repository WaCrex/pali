/********************************* Includes **********************************/
#include "Arduino.h"
#include "Blinky.h"

#include <SimpleTest.h>

/****************************** Example settings *****************************/

/**************************** Constants and Types ****************************/
#define EXPECTED_DIGITAL_WRITES 4
typedef struct
{
    uint8_t Pin;
    uint8_t Value;
} DigitalWrite_t;

/********************************* Variables *********************************/
static DigitalWrite_t m_digital_writes[EXPECTED_DIGITAL_WRITES];
static uint8_t m_digital_writes_count;
static unsigned long m_requested_delay;

/************************* Local Function Prototypes *************************/
static void reset_local_variables();

/***************************** Exported Functions ****************************/
SimpleTest_Initialize();
SimpleTest_CreateTest(BlinkOnce_Sequence)
{
    // Prepare our data to be monitored
    reset_local_variables();

    // Call the component, this will
    // run the complete BlinkOnce logic and return
    Blinky_BlinkOnce();

    // What should we check against here?
    SimpleTest_AssertInteger(HIGH, m_digital_writes[0].Value, "Pin is not HIGH!");
    SimpleTest_AssertInteger(HIGH, m_digital_writes[1].Value, "Pin is not HIGH!");
    SimpleTest_AssertInteger(200, m_requested_delay, "Not correct delay!");
    SimpleTest_AssertInteger(LOW, m_digital_writes[2].Value, "Pin is not LOW!");
    SimpleTest_AssertInteger(LOW, m_digital_writes[3].Value, "Pin is not LOW!");
}
SimpleTest_FinalizeTest()

static char* runTests()
{
    SimpleTest_RunTest(BlinkOnce_Sequence);
    return NULL;
}

int main(int argc, char **argv)
{
    const char* testsResult =  runTests();
    printf("%s\n", (testsResult == NULL) ? "All tests passed" : testsResult);
    printf("Tests run: %d\n", SimpleTestCounter);
    return (testsResult != NULL);
}

/****************************** Local Functions ******************************/
static void reset_local_variables()
{
    m_requested_delay = 0;
    m_digital_writes_count = 0;
    for (int n = 0; n<EXPECTED_DIGITAL_WRITES; ++n) {
        m_digital_writes[n].Pin = 255;
        m_digital_writes[n].Value = 255;
    }
}

void digitalWrite(uint8_t pin, uint8_t val)
{
    m_digital_writes[m_digital_writes_count].Pin = pin;
    m_digital_writes[m_digital_writes_count].Value = val;
    ++m_digital_writes_count;
}

void delay(unsigned long milliseconds)
{
    m_requested_delay = milliseconds;
}
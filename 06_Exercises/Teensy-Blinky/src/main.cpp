/********************************* Includes **********************************/
#include "Arduino.h"
#include "LiquidCrystal.h"

/****************************** Example settings *****************************/
#define EnableSlowInterrupts  0
#define EnableCriticalSection 1
#define EnableDebouncing      1

/**************************** Constants and Types ****************************/
#define BUTTON_PIN             0u
#define DisplayRefreshRateMS   100u
#define DebounceConstantMin    0
#define DebounceConstantMax    100

#define EnterCriticalSection() noInterrupts()
#define LeaveCriticalSection() interrupts()

/********************************* Variables *********************************/
static LiquidCrystal m_display(2, 3, 4, 5, 6, 7);
static uint32_t m_nextDrawTimestamp;
static volatile uint16_t m_interruptCalls, m_ledUpdates;
static volatile uint8_t m_currentButtonState; 
static uint8_t m_debounceTicks;

/************************* Local Function Prototypes *************************/
static void handleButtonInterrupt(void);
static void setLEDState(uint8_t state);
static bool shouldUpdateDisplay(void);
static void updateDisplay(void);

/***************************** Exported Functions ****************************/
void setup()
{
  m_nextDrawTimestamp = 0u;
  m_interruptCalls = 0u;
  m_ledUpdates = 0u;
  m_debounceTicks = ((DebounceConstantMax-DebounceConstantMin)/2u);
  m_currentButtonState = LOW;
  m_display.begin(16, 2);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), handleButtonInterrupt, CHANGE);
}

void loop()
{
  #if EnableDebouncing == 1
  if (m_currentButtonState) {
    if (m_debounceTicks < DebounceConstantMax) {
      ++m_debounceTicks;
      if (m_debounceTicks == DebounceConstantMax) {
        setLEDState(LOW);
      }
    }
  } else {
    if (m_debounceTicks > DebounceConstantMin) {
      --m_debounceTicks;
      if (m_debounceTicks == DebounceConstantMin) {
        setLEDState(HIGH);
      }
    }
  }
  #endif
  if (shouldUpdateDisplay()) {
    updateDisplay();
    m_nextDrawTimestamp = (millis() + DisplayRefreshRateMS);
  }
  delay(1);
}

/****************************** Local Functions ******************************/
static void handleButtonInterrupt()
{
  #if EnableDebouncing == 0
  setLEDState(!digitalRead(BUTTON_PIN));
  #else
  m_currentButtonState = digitalRead(BUTTON_PIN);
  #endif
  ++m_interruptCalls;

  #if EnableSlowInterrupts == 1
  delay(1000u);
  #endif
}

static void setLEDState(uint8_t state)
{
  static uint8_t lastState = 0xFFu;
  if (lastState != state) {
    digitalWrite(LED_BUILTIN, state);
    ++m_ledUpdates;
    lastState = state;
  }
}

static bool shouldUpdateDisplay(void)
{
  return (millis() >= m_nextDrawTimestamp);
}

static void updateDisplay(void)
{
  static uint8_t tickAnimation[] = {'|', '/', '-', '\\' };
  static uint8_t tickAnimationIndex = 0u;
  tickAnimationIndex = ((tickAnimationIndex+1u) >= sizeof(tickAnimation)) ? 0u : (tickAnimationIndex+1u);

  #if EnableCriticalSection == 1
  uint32_t interruptCalls;
  EnterCriticalSection();
  interruptCalls = m_interruptCalls;
  LeaveCriticalSection();

  m_display.setCursor(2, 0);
  m_display.printf("%cInterrupts%c", tickAnimation[tickAnimationIndex], tickAnimation[tickAnimationIndex]);
  m_display.setCursor(3, 1);
  m_display.printf("%04d  %04d", interruptCalls, m_ledUpdates);
  #else
  m_display.setCursor(2, 0);
  m_display.printf("%cInterrupts%c", tickAnimation[tickAnimationIndex], tickAnimation[tickAnimationIndex]);
  m_display.setCursor(3, 1);
  m_display.printf("%04d  %04d", m_interruptCalls, m_ledUpdates);
  #endif
}

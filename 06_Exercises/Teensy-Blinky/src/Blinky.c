/********************************* Includes **********************************/
#include "Arduino.h"
#include "Blinky.h"

/****************************** Example settings *****************************/

/**************************** Constants and Types ****************************/
#define PIN_A 0
#define PIN_B 1
#define BLINK_ONCE_DELAY 200

/********************************* Variables *********************************/

/************************* Local Function Prototypes *************************/

/***************************** Exported Functions ****************************/
void Blinky_BlinkOnce()
{
    digitalWrite(PIN_A, HIGH);
    digitalWrite(PIN_B, HIGH);
    delay(BLINK_ONCE_DELAY);
    digitalWrite(PIN_A, LOW);
    digitalWrite(PIN_B, LOW);
}


/****************************** Local Functions ******************************/

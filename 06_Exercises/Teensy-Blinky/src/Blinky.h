#ifndef BLINKY_H
#define BLINKY_H

#ifdef __cplusplus
extern "C" {
#endif

void Blinky_BlinkOnce();
void Blinky_BlinkTwice();
void Blinky_BlinkTimes(int times);

#ifdef __cplusplus
}
#endif

#endif
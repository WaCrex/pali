#!/usr/bin/env bash

# From the scripts location, go back one step and mark this location as REPO_ROOT
REPO_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && cd .. && pwd )"
PLATFORM_IO_ROOT=$REPO_ROOT

pushd $PLATFORM_IO_ROOT
platformio run
PLATFORM_IO_RESULT=$?
popd

# Exit with the same result as build test
exit $PLATFORM_IO_RESULT
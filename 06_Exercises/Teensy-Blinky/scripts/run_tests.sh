#!/usr/bin/env bash
# From the scripts location, go back one step and mark this location as REPO_ROOT
# We then use the REPO_ROOT location to build up our test location TESTS_ROOT
REPO_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && cd .. && pwd )"
TESTS_ROOT=$REPO_ROOT/test

# Navigate to the tests location
pushd $TESTS_ROOT > /dev/null
# Run make to build the test binary
make > /dev/null
# Find the binary in the build folder and run it
TEST_BINARY=`find build -type f -perm -u+x`
./$TEST_BINARY
# Store the result fromt he test
TEST_RESULT=$?
popd > /dev/null

# Exit with the same result as the binary.
exit $TEST_RESULT


